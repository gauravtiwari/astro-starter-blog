---
title: 'Introducing Astro: Ship Less JavaScript'
description: "We're excited to announce Astro as a new way to build static websites and deliver lightning-fast performance without sacrificing a modern developer experience."
publishDate: 'Tuesday, June 8 2021'
author: 'jed'
heroImage: '/social.jpg'
alt: 'Astro'
layout: '../../layouts/BlogPost.astro'
---

**Unfortunately**, this is more modern web development has been trending in the opposite direction—towards *more.* More JavaScript, more features, more moving parts, and ultimately more complexity needed to keep it all running smoothly.

Probably, the biggest thing people don't understand about the process is the importance of expressing yourself clearly. Every year we get some applications that are obviously good, some that are obviously bad, and in the middle a huge number where we just can't tell. The idea seems kind of promising, but it's not explained well enough for us to understand it. The founders seem like they might be good, but we don't get a clear enough picture of them to say for sure.

> check this out

Today I'm excited to publicly share Astro: a new kind of static site builder that delivers lightning-fast performance:

* **Bring Your Own Framework (BYOF):** Build your site using React, Svelte, Vue, Preact, web components, or just plain ol' HTML + JavaScript.

* **100% Static HTML, No JS:** Astro renders your entire page to static HTML, removing all JavaScript from your final build by default.

* **On-Demand Components:** Need some JS? Astro can automatically hydrate interactive components when they become visible on the page. If the user never sees it, they never load it.

* **Fully-Featured:** Astro supports TypeScript, Scoped CSS, CSS Modules, Sass, Tailwind, Markdown, MDX, and any of your favorite npm packages.

* **SEO Enabled:** Automatic sitemaps, RSS feeds, pagination and collections take the pain out of SEO and syndication.

This post marks the first public beta release of Astro. **Missing features and bugs are still to be expected at this early stage.** There are still some months to go before an official 1.0 release, but there are already several fast sites built with Astro in production today.

> To learn more about Astro and start building your first site, check out [the project README.](https://github.com/snowpackjs/astro#-guides).

## Getting Started

Starting a new project in Astro is easy works more:

```shell
# create your project
mkdir new-project-directory
cd new-project-directory
npm init astro

# install your dependencies
npm install

# start the dev server and open your browser
npm run dev
```

## How Astro Works in real world

Astro works a lot like a static site generator. If you have ever used Eleventy:

you work out compose your website using UI components from your favorite JavaScript web framework (React, Svelte, Vue, etc). Astro renders your entire site to static HTML during the build. The result is a fully static website with all JavaScript removed from the final page. No monolithic JavaScript application required, just static HTML that loads as fast as possible in the browser regardless of how many UI components you used to generate it.

Like all investors, we want to believe. So help us believe. If there's something about you that stands out, or some special insight you have into the problem you plan to work on, make sure we see it.

The best way to do that is simply to be concise. You don't have to sell us on you. We'll sell ourselves, if we can just understand you. But every unnecessary word in your application subtracts from the effect of the necessary ones. So before submitting your application, print it out and take a red pen and cross out every word you don't need. And in what's left be as specific and as matter-of-fact as you can.

Of course, sometimes client-side JavaScript is inevitable. Image carousels, shopping carts, and auto-complete search bars are just a few examples of things that require some JavaScript to run in the browser. This is where Astro really shines: When a component needs some JavaScript, Astro only loads that one component (and any dependencies). The rest of your site continues to exist as static, lightweight HTML.

In other full-stack web frameworks this level of per-component optimization would be impossible without loading the entire page in JavaScript, delaying interactivity. In Astro, this kind of [partial hydration](https://addyosmani.com/blog/rehydration/) is built into the tool itself.

This new approach to web architecture is called [islands architecture](https://jasonformat.com/islands-architecture/). We didn't coin the term, but Astro may have perfected the technique. We are confident that an HTML-first, JavaScript-only-as-needed approach is the best solution for the majority of content-based websites.

> To learn more about Astro and start building your first site, check out [the project README.](https://github.com/snowpackjs/astro#-guides)

## Embracing the Pit of Success

> A well-designed system makes it easy to do the right things and annoying (but not impossible) to do the wrong things<div class="source"><p>– Jeff Atwood</p>[Falling Into The Pit of Success](https://blog.codinghorror.com/falling-into-the-pit-of-success/)</div>

Poor performance is often framed as a failure of the developer, but we respectfully disagree. In many cases, poor performance is a failure of tooling. It should be difficult to build a slow website.

Astro's main design principle is to lead developers into what [Rico Mariani](https://twitter.com/ricomariani) dubbed "the pit of success". It is our goal to build every site "fast by default" while also delivering a familiar, modern developer experience.

By building your site to static HTML by default, Astro makes it difficult (but never impossible 😉) to build a slow site.

## Long-Term Sustainability

Astro is built by the team of open source developers behind [Snowpack](https://snowpack.dev) and [Skypack](https://skypack.dev), with additional contributions from the community.

**Astro is and always will be free.** It is an open source project released under the [MIT license](https://github.com/snowpackjs/astro/blob/main/LICENSE).

If the founders seem promising but the idea doesn't, I check the question near the end that asks what other ideas the founders had. It's quite common for us to fund groups to work on ideas they listed as alternates.

> Please tell us about the time you most successfully hacked some (non-computer) system to your advantage.

If this wasn't already clear, we're not looking for the sort of obedient, middle-of-the-road people that big companies tend to hire. We're looking for people who like to beat the system. So if the answer to this question is good enough, it will make me go back and take a second look at an application that otherwise seemed unpromising. In fact, I think there are people we've invited to interviews mainly on the strength of their answer to this question.

We care deeply about building a more sustainable future for open source software. At the same time, we need to support Astro's development long-term. This requires money (donations alone aren't enough.)

We're inspired by the early success of projects like [Tailwind](https://tailwindcss.com/), [Rome](https://rome.tools/), [Remix](https://remix.run/), [Ionic](https://ionicframework.com/), and others who are experimenting with long-term financial sustainability on top of Open Source. Over the next year we'll be exploring how we can create a sustainable business to support a 100% free, open source Astro for years to come.

If your company is as excited about Astro as we are, [we'd love to hear from you.](https://astro.build/chat)

Finally, I'd like to give a **HUGE** thanks to the 300+ developers who joined our earliest private beta. Your feedback has been essential in shaping Astro into the tool it is today. If you're interested in getting involved (or just following along with development) please [join us on Discord.](https://astro.build/chat)

> To learn more about Astro and start building your first site, check out [the project README.](https://github.com/snowpackjs/astro#-guides)